<?php

/**
 * @file
 * Time based Registration admin file.
 */

/**
 * Time based registration configuration form.
 */
function time_based_registration_settings_form($form, &$form_state) {

  // Incorporating date css
  $form['#attached']['css'] = array(
    module_exists('date') ? drupal_get_path('module', 'date') . '/date_api/date.css' :'',
  );
    
  $form['tbr_config'] = array(
    '#type' => 'fieldset',
    '#title' => t('Time based Registration Setting'),
    '#collapsible' => FALSE,
    '#tree' => TRUE,
  );
  $form['tbr_config']['current_site_time_zone'] = array(
    '#markup' => t('Site Time zone is: <a href="@tbr-timezone">@tbr-link</a><br />', array(
      '@tbr-timezone' => url('admin/config/regional/settings'),
      '@tbr-link' => date_default_timezone_get(),

    )),
  );
  $tbr_config = variable_get('tbr_config');
  $time_based_from = (isset($tbr_config['from']) ? $tbr_config['from'] : '');
  $time_based_to = (isset($tbr_config['to']) ? $tbr_config['to'] : '');

  $form['tbr_config']['event'] = array(
    '#markup' => t('When do you want to open Registration?'),
  );
  $form['tbr_config']['from'] = array(
    '#type' => 'date_select',
    '#title' => t('From date'),
    '#default_value' => isset($time_based_from) ? $time_based_from : '',
    '#date_year_range' => '0:+1',
  );
  $form['tbr_config']['to'] = array(
    '#type' => 'date_select',
    '#title' => t('To date'),
    '#default_value' => isset($time_based_to) ? $time_based_to : '',
    '#date_year_range' => '0:+1',
  );

  $form['#submit'][] = 'time_based_registration_form_submit';
  $form['#validate'][] = 'time_based_registration_configuration_validate';

  return system_settings_form($form);
}

/**
 * Validating the time_based_registration configuration form.
 */
function time_based_registration_configuration_validate($form, &$form_state) {

  $from_time = strtotime($form_state['values']['tbr_config']['from']);
  $to_time = strtotime($form_state['values']['tbr_config']['to']);

  if ($from_time >= $to_time) {
    form_set_error($from_time, t('From date should not be greater than To date.'));
  }
  elseif ($from_time == '' ||  $to_time == '') {
    form_set_error($from_time, t('Enter the From date and To date'));
  }
}

/**
 * Regustration form submit function.
 */
function time_based_registration_form_submit($form, &$form_state) {
  $from_time = strtotime($form_state['values']['tbr_config']['from']);
  $to_time = strtotime($form_state['values']['tbr_config']['to']);
  $time = round(($to_time - $from_time) / 60);
  $d = floor($time / 1440);
  $h = floor(($time - $d * 1440) / 60);
  $m = $time - ($d * 1440) - ($h * 60);
  drupal_set_message("Registration is available for $d day  $h hour   $m minute");
}
